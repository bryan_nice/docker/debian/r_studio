FROM debian:11.6 AS R_STAGE

RUN apt-get update \
 && apt-get install -y \
      gnupg2 \
 && apt-key adv --keyserver keyserver.ubuntu.com \
    --recv-key '95C0FAF38DB3CCAD0C080A7BDC78B2DDEABC47B7' \
 && echo "deb http://cloud.r-project.org/bin/linux/debian bullseye-cran40/" > \
    /etc/apt/sources.list.d/cran-r.list \
 && apt-get update \
 && apt-get install -y \
        pandoc \
        r-base \
        r-cran-tinytex

# Install R Studio
RUN apt-get update \
 && apt-get install -y \
      gdebi-core \
      libcairo2-dev \
      libcurl4-openssl-dev \
      libfontconfig1-dev \
      libgdal-dev  \
      libgeos-dev \
      libproj-dev  \
      libssl-dev \
      libudunits2-dev \
      libxml2-dev \
      wget \
 && wget https://download2.rstudio.org/server/jammy/amd64/rstudio-server-2022.12.0-353-amd64.deb \
 && gdebi -n rstudio-server-2022.12.0-353-amd64.deb \
 && rm -rf rstudio-server-2022.12.0-353-amd64.deb

# Clean Up apt-get cache
RUN apt-get clean \
 && rm -rf /var/lib/apt/lists/* \
 && rm -rf /var/lib/rstudio-server/r-versions

COPY sssd.conf /etc/sssd/sssd.conf
COPY logging.conf /etc/rstudio/logging.conf

# Set default env values
ENV RSW_LICENSE ""
ENV RSW_LICENSE_SERVER ""
ENV RSW_TESTUSER rstudio
ENV RSW_TESTUSER_PASSWD rstudio
ENV RSW_TESTUSER_UID 10000
ENV RSW_LAUNCHER true
ENV RSW_LAUNCHER_TIMEOUT 10
ENV DIAGNOSTIC_DIR /var/log/rstudio
ENV DIAGNOSTIC_ENABLE false
ENV DIAGNOSTIC_ONLY false

RUN useradd -m rstudio \
 && echo "rstudio:rstudio" | chpasswd \
 && mkdir -p /home/rstudio/.rstudio/monitored/user-settings \
 && chown -R rstudio:rstudio /home/rstudio/.rstudio

# Create log dir
RUN mkdir -p /var/lib/rstudio-server/monitor/log \
 && chown -R rstudio:rstudio /var/lib/rstudio-server/monitor \
 && chmod 600 /etc/sssd/sssd.conf \
 && echo '\n# allow home directory creation\nsession required pam_mkhomedir.so skel=/etc/skel umask=0022' >> /etc/pam.d/common-session

RUN echo "tail -f /dev/null" >> /usr/sbin/rstudio-server

RUN R -e "install.packages('devtools', dependencies = TRUE)"

RUN R -e "install.packages( \
    c( \
      'diagram' \
      , 'dplyr' \
      , 'epir' \
      , 'ggplot2' \
      , 'ggrepel' \
      , 'kableExtra' \
      , 'knitr' \
      , 'pander' \
      , 'rmarkdown' \
      , 'stringr' \
      , 'tidyverse' \
      , 'tinytex' \
    ), \
    dependencies = TRUE, \
    repos = c( \
      'https://cloud.r-project.org' \
    ) \
)"

USER rstudio
RUN R -e "tinytex::install_tinytex()"

USER root

ENTRYPOINT ["rstudio-server","start"]
